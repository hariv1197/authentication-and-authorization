package com.myapp.authenticate.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.authenticate.models.ERole;
import com.myapp.authenticate.models.Role;
import com.myapp.authenticate.models.User;
import com.myapp.authenticate.payload.request.LoginRequest;
import com.myapp.authenticate.payload.request.SignupRequest;
import com.myapp.authenticate.payload.response.JwtResponse;
import com.myapp.authenticate.payload.response.MessageResponse;
import com.myapp.authenticate.repository.RoleRepository;
import com.myapp.authenticate.repository.UserRepository;
import com.myapp.authenticate.security.jwt.JwtUtils;
import com.myapp.authenticate	.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		String strRoles = signUpRequest.getUserrole();
		Set<Role> roles = new HashSet<>();

		if (strRoles.equals(null)) {
			Optional<Role> userRole = roleRepository.findByName(ERole.ROLE_USER);
			roles.add(userRole.get());
		} else {
			switch (strRoles.toLowerCase()) {
			case "admin":
				Optional<Role> adminRole = roleRepository.findByName(ERole.ROLE_ADMIN);
				if (adminRole != null) {
					roles.add(adminRole.get());
				} else {
					roles.add(new Role(ERole.ROLE_ADMIN));
				}
				break;
			default:
				Optional<Role> userRole = roleRepository.findByName(ERole.ROLE_USER);
				if (userRole != null) {
					roles.add(userRole.get());
				} else {
					roles.add(new Role(ERole.ROLE_USER));
				}
				}
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
